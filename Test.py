import unittest
from Utilities import Utilities 
from Game import Game
from Player import Player

class MorpionTest(unittest.TestCase):
	"""
		color print variables
	"""
	BLACK			= "\033[0;30m" 
	RED				= "\033[0;31m" 
	GREEN			= "\033[0;32m" 
	BROW_ORANGE		= "\033[0;33m" 
	BLUE			= "\033[0;34m"
	PURPLE			= "\033[0;35m" 
	CYAN			= "\033[0;36m"
	LIGHT_GREY		= "\033[0;37m"
	
	BLACK_GREY		= "\033[1;30m" 
	LIGHT_RED		= "\033[1;31m" 
	LIGHT_GREEN		= "\033[1;32m" 
	YELLOW			= "\033[1;33m" 
	LIGHT_BLUE		= "\033[1;34m"
	LIGHT_PURPLE	= "\033[1;35m" 
	LIGHT_CYAN		= "\033[1;36m"
	WHITE			= "\033[1;37m"

	NC				= "\033[0m"
	
	def setUp(self): 
		self.player_1  = Player(name="player 1", SIGN="X")
		self.player_2  = Player(name="player 2", SIGN="O")
		self.game = Game(self.player_1, self.player_2)
		self.game.initialize_grid()

	def test_four_in_row(self):
		bad_row = ['O','X','X','O','X','X','O','X','O','X','X','X','O',]
		good_row_player_1 = ['O','O','X','X','X','X','O','O','X','O','X','X','O',]
		good_row_player_2 = ['O','O','X','O','X','X','O','O','O','O','X','X','O',]

		self.assertFalse(False, Utilities.four_in_row(bad_row, self.player_1) )
		self.assertFalse(False, Utilities.four_in_row(bad_row, self.player_2) )
		self.assertTrue(True, Utilities.four_in_row(good_row_player_1, self.player_1))
		self.assertTrue(True, Utilities.four_in_row(good_row_player_2, self.player_2))

	def test_get_colum_list(self):
		grid = [
			[1,2,3],
			[4,5,6],
			[7,8,9],
		]
		expected_column_list= [
			[1,4,7],
			[2,5,8],
			[3,6,9],
		]
		self.assertEqual(expected_column_list, Utilities.get_column_list(grid))

	def test_index_column(self):
		valid_list_index = {"a": 0, "b": 1, "c": 2, "d":3, "e":4, "f":5, "g":6}
		for k,v in valid_list_index.items():
			self.assertEqual(v, Utilities.char_to_index(k))

	def test_valid_column(self):
		self.game._COLUMN = 26
		self.game.initialize_grid()
		valid_columns = ["a", "B", "c", "d", "E", "F", "g","h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v","w", "x","y", "z"]
		invalid_columns	  = ["1","2","3","4","5","6"]
		for c in valid_columns:
			coordinate = self.game.valid_column(c)['index']
			self.assertIsNotNone(coordinate)
			self.assertEqual([self.game.ROW-1, Utilities.char_to_index(c)], coordinate ) # test coordinate of the box

		for c in invalid_columns:
			self.assertIsNone(self.game.valid_column(c)['index'])



	def test_set_grid(self):
		self.game.set_grid([0,0], self.player_1)
		self.assertEqual(self.player_1.SIGN, self.game.grid[0][0])


	def test_get_good_column_letters(self):
		self.game._COLUMN = 26
		actual_column_list = Utilities.get_good_column_letters(self.game)
		expected_column_list = ["A", "B", "C", "D", "E", "F", "G","H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V","W", "X","Y", "Z"]
		self.assertEqual(expected_column_list, actual_column_list)

	def test_grid_is_full(self):
		
		self.game.grid = [
			["X","X","X","O","X","O","X",],
			["X","O","X","X","O","X","X",],
			["X","X","X","O","X","X","X",],
			["O","X","O","X","O","O","X",],
			["X","O","X","X","X","X","X",],
			["X","X","O","X","X","O","X",],
		]
		self.assertTrue(self.game.grid_is_full())
		self.game.grid = [
			["-","-","-","-","-","-","-",],
			["-","-","-","-","-","-","-",],
			["-","X","-","-","X","-","-",],
			["O","X","-","X","O","-","-",],
			["X","O","X","X","X","-","X",],
			["X","X","O","X","X","-","X",],
		]
		self.assertFalse(self.game.grid_is_full())

	def test_win_horizontally(self):
		self.game.grid = [
			["-","-","-","-","-","-","-",],
			["-","-","-","-","-","-","-",],
			["-","-","-","-","-","-","-",],
			["O","X","-","-","X","-","-",],
			["X","O","X","-","O","O","X",],
			["X","X","O","X","X","X","X",],
		]
		
		expected = {"player": self.player_1, "direction": "horizontally"}
		self.assertEqual(expected, self.game.win(self.player_1))
		
		self.game.grid = [
			["-","-","-","-","-","-","-",],
			["O","-","-","-","-","-","-",],
			["O","-","-","-","-","-","-",],
			["O","X","-","O","O","O","O",],
			["X","O","X","O","O","X","X",],
			["X","X","O","X","O","X","X",],
		]
		expected = {"player": self.player_2, "direction": "horizontally"}
		self.assertEqual(expected, self.game.win(self.player_2))

	def test_win_vertically(self):
		self.game.grid = [
			["-","-","-","-","-","-","-",],
			["-","-","-","-","-","-","-",],
			["-","-","-","-","X","-","-",],
			["O","X","-","-","X","-","-",],
			["X","O","X","-","X","O","X",],
			["X","X","O","O","X","O","X",],
		]
		
		expected = {"player": self.player_1, "direction": "vertically"}
		self.assertEqual(expected, self.game.win(self.player_1))
		
		self.game.grid = [
			["-","-","-","-","-","-","-",],
			["-","-","-","-","-","-","-",],
			["-","-","-","-","-","O","-",],
			["O","X","-","-","X","O","-",],
			["X","O","X","-","O","O","X",],
			["X","X","O","O","X","O","X",],
		]
		expected = {"player": self.player_2, "direction": "vertically"}
		self.assertEqual(expected, self.game.win(self.player_2))

	def test_win_left_diagonally(self):
		self.game.grid = [
			["-","-","-","-","-","-","-",],
			["-","-","-","-","-","-","-",],
			["X","-","-","-","-","-","-",],
			["-","X","-","-","-","-","-",],
			["-","-","X","-","-","-","-",],
			["-","-","-","X","-","-","-",],
		]
		expected = {"player": self.player_1, "direction": "left diagonally"}
		self.assertEqual(expected, self.game.win(self.player_1))
		
		self.game.grid = [
			["-","-","-","-","-","-","-",],
			["-","-","-","-","-","-","-",],
			["O","-","-","-","-","-","-",],
			["X","O","-","-","-","-","-",],
			["X","X","O","-","-","-","-",],
			["X","X","X","O","-","-","-",],
		]
		expected = {"player": self.player_2, "direction": "left diagonally"}
		self.assertEqual(expected, self.game.win(self.player_2))

	def test_win_right_diagonally(self):
		self.game.grid = [
			["-","-","-","-","-","-","-",],
			["-","-","-","-","-","-","-",],
			["-","-","-","-","-","-","X",],
			["-","-","-","-","-","X","O",],
			["-","-","-","-","X","O","X",],
			["-","-","-","X","O","O","O",],
		]
		expected = {"player": self.player_1, "direction": "right diagonally"}
		self.assertEqual(expected, self.game.win(self.player_1))
		
		self.game.grid = [
			["-","-","-","-","-","-","-",],
			["-","-","-","-","-","-","-",],
			["-","-","-","-","-","O","-",],
			["-","-","-","-","O","X","-",],
			["-","-","-","O","X","O","-",],
			["-","-","O","X","O","X","X",],
		]
		expected = {"player": self.player_2, "direction": "right diagonally"}
		self.assertEqual(expected, self.game.win(self.player_2))


	def _tab(self):
		default_tab = "\t\t"
		length = len(self._testMethodName)
		if length > 21:
			return default_tab
		elif length == 21:
			return default_tab+"\t"
		elif length < 15:
			return default_tab+ "\t\t"
		else:
			return default_tab+"\t"


	def __str__(self):
		return self.BROW_ORANGE+ "%s %s\033[0m" % (self._testMethodName, self._tab())
		

	def tearDown(self):
		del self

if __name__ == '__main__':
	#unittest.colour_runner.runner.ColourTextTestRunner()
    
    unittest.main(verbosity=2)