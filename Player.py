from Utilities import Utilities
from random import choice
class Player():


	def __init__(self, name, SIGN, computer=False):
		self._name = name
		self._score = 0
		self.computer = computer
		self.SIGN = SIGN

	def get_name(self):
		return self._name

	def __str__(self):
		return self._name + ", SIGN = "+ self.SIGN + ", Score = "+ str(self._score)

	@property
	def increment_score(self):
		self._score += 1

	@property
	def decrement_score(self):
		self._score -= 1

	@property
	def score(self):
		return self._score


	def propose_column(self, game):
		if self.computer:
			return self.john_doe_propose_column(game)
		valid_column = {"index":None}
		while valid_column['index'] == None:
			column = input(self._name +" propose a column to play (ex:A or B or c etc...)\n>>> ").lower()
			valid_column = game.valid_column(column)
		return valid_column['index']


	def john_doe_propose_column(self, game):
		columns = Utilities.get_good_column_letters(game)
		column = choice(columns)
		valid_column = game.valid_column(column)
		if not valid_column['index'] :
			return john_doe_propose_colmn(self, game)
		return valid_column['index']
