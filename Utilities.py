class Utilities():

	@classmethod
	def get_good_column_letters(cls, game):
		a = ord("a")
		good_letters = []
		for i in range(game.COLUMN):
			good_letters.append( chr(a+i).upper() )

		return good_letters

	@classmethod
	def display_column_letters(cls, game, espace=""):
		column_letters = Utilities.get_good_column_letters(game)
		for letter in column_letters:
			print(letter, end=espace)
		print()

	@classmethod
	def char_to_index(cls, char ):
		start = ord("a")
		return ord(char.lower()) - start


	@classmethod
	def get_column_list(cls, grid):
		column_list = []
		for i in range(len(grid)):
			tmp_column = []
			for j in range(len(grid[i])):
				tmp_column.append(grid[j][i])
			column_list.append(tmp_column)
		return column_list


	@classmethod
	def four_in_row(cls, tab, player):

		counter = 0
		for s in tab:

			if s.upper() == player.SIGN.upper():
				counter +=1
			else:
				counter = 0
			if counter == 4:
				return True
		return False 


	@classmethod
	def get_all_left_diagonally(cls, game):
		nb_row_in_diagonal = game.ROW - 3
		nb_column_in_diagonal = (game.COLUMN - 3)
		
		all_diagonals = []

		for i in reversed(range(nb_row_in_diagonal)):
			index_counter = 0
			tmp_diagonal = []
			for j in range(i, game.ROW):
				tmp_diagonal.append(game.grid[j][index_counter])
				index_counter += 1
			all_diagonals.append(tmp_diagonal)

		for i in range(nb_column_in_diagonal-1, 0, -1):
			index_counter = 0
			tmp_diagonal = []
			for j in range(i, game.COLUMN):
				tmp_diagonal.append(game.grid[index_counter][j])
				index_counter += 1
			all_diagonals.append(tmp_diagonal)


		return all_diagonals

	@classmethod
	def get_all_right_diagonally(cls, game):
		nb_row_in_diagonal = game.ROW - 3
		nb_column_in_diagonal = (game.COLUMN - 3)
		
		all_diagonals = []
		start = nb_row_in_diagonal - game.ROW
		for i in range(3,game.ROW):
			index_counter = 0
			tmp_diagonal = []
			for j in reversed(range(i+1)):
				tmp_diagonal.append(game.grid[j][index_counter])
				index_counter += 1
			all_diagonals.append(tmp_diagonal)

		for i in range(1, nb_column_in_diagonal):
			index_counter = game.ROW -1
			tmp_diagonal = []
			for j in range(i, game.COLUMN):
				tmp_diagonal.append(game.grid[index_counter][j])
				index_counter -= 1
			all_diagonals.append(tmp_diagonal)

		return all_diagonals
