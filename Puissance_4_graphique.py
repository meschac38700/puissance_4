from tkinter import *
from tkinter import ttk
from tkinter.colorchooser import *
from Player import Player
from Game import Game
class Puissance4Graphique():
	TITLE  = "Puissance 4"

	def __init__(self, player_1, player_2, player_1_color, player_2_color ,who_start):

		self.player_1 = Player(name=player_1, SIGN="X")
		self.player_2 = Player(name=player_2, SIGN="O")
		self.player_1_color = player_1_color
		self.player_2_color = player_2_color
		self.who_plays = who_start
		self.game = Game(self.player_1, self.player_2, p_row=10, p_col=11 )
		self.mainFrame = ttk.PanedWindow(name="mainframe", width=self.set_width(), height=self.set_height(), orient=VERTICAL)
		self.mainFrame.pack(fill=BOTH, expand=True)

		self.topbar = PanedWindow(self.mainFrame, name="topbar", height=52, orient=VERTICAL, bg="#b2bec3")
	
		self.mainFrame.add(self.topbar)
		self.topbar.add(Label(self.topbar, text="Puissance 4", bg="#b2bec3"))

		self.body = ttk.PanedWindow(self.mainFrame, name="body", orient=HORIZONTAL)
		self.mainFrame.add(self.body, weight=1)

		self.footer = ttk.PanedWindow(self.mainFrame, height=50, name="footer", orient=VERTICAL)
		self.mainFrame.add(self.footer)

		self.btn = Button(self.footer, text="Rejouer",  width=20,fg="#0984e3", bd=2, font="Helvetica 10 bold", command=self.initializeGrid )
		self.footer.add(self.btn)

		self.body_leftSide= ttk.PanedWindow(self.body , width=200, name="body_leftSide", orient=VERTICAL)
		self.body.add(self.body_leftSide)

		self.body_center = ttk.PanedWindow(self.body, name="body_center")
		self.body.add(self.body_center, weight=1)

		self.body_rightSide = ttk.PanedWindow(self.body, orient=VERTICAL, width=300, name="body_rightSide")
		self.body.add(self.body_rightSide)

		self.body_leftSide_top = ttk.PanedWindow(self.body_leftSide, name="body_leftSide_top", orient=HORIZONTAL)
		self.body_leftSide.add(self.body_leftSide_top, weight=1)

		self.body_leftSide_bottom = ttk.PanedWindow(self.body_leftSide, name="body_leftSide_bottom", orient=HORIZONTAL)
		self.body_leftSide.add(self.body_leftSide_bottom, weight=1)

		self.player_1_label = Label(self.body_leftSide_top, text=self.player_1.get_name())
		self.body_leftSide_top.add(self.player_1_label,weight=1)


		self.player_2_label = Label(self.body_leftSide_bottom, text=self.player_2.get_name())
		self.body_leftSide_bottom.add(self.player_2_label,weight=1)

		self.body_leftSide_top.add(Frame(self.body_leftSide_top, width=40, bg=self.player_1_color, padx=(10)))
		self.body_leftSide_bottom.add(Frame(self.body_leftSide_bottom, width=40, bg=self.player_2_color))

		self.body_rightSide_top = ttk.PanedWindow(self.body_rightSide, name="body_rightSide_top", orient=HORIZONTAL)
		self.body_rightSide.add(self.body_rightSide_top, weight=1)
		self.player_1_label_score = Label(self.body_rightSide_top, font="Helvetica 10 bold", text=self.player_1.get_name() +"'s score : ")
		self.body_rightSide_top.add(self.player_1_label_score, weight=1)

		self.body_rightSide_bottom = ttk.PanedWindow(self.body_rightSide, name="body_rightSide_bottom", orient=HORIZONTAL)
		self.body_rightSide.add(self.body_rightSide_bottom, weight=1)
		self.player_2_label_score = Label(self.body_rightSide_bottom, font="Helvetica 10 bold", text=self.player_2.get_name() +"'s score : ")
		self.body_rightSide_bottom.add(self.player_2_label_score, weight=1)
		
		self.player_1_score = Label(self.body_rightSide_top, font="Helvetica 30 bold", fg="#ff0000",text= self.player_1.score, padx=50)
		self.player_2_score = Label(self.body_rightSide_bottom,font="Helvetica 30 bold", fg="#ff0000", text= self.player_2.score, padx=50)

		self.body_rightSide_top.add(self.player_1_score)
		self.body_rightSide_bottom.add(self.player_2_score)

		self.initializeGrid()

	def set_width(self):
		if self.game.ROW == 6 :
			width = 985
		elif self.game.ROW < 6:
			width = 985 - (82*(6-self.game.ROW))
		else:
			width = 985 + (82*(self.game.ROW-6))
		return width

	def set_height(self):
		if self.game.COLUMN == 7:
			height = 400
		elif self.game.COLUMN < 7:
			height = 400 - (50*(7-self.game.COLUMN))
		else:
			height = 400 + (50*(self.game.COLUMN-7))
		return height

	def initializeGrid(self):
		self.game.initialize_grid()
		self.player_1_score.config(text= self.player_1.score)
		self.player_2_score.config(text = self.player_2.score)
		for ligne in range(self.game.ROW):
			for colonne in range(self.game.COLUMN):
				btn = Button(self.body_center, text=self.game._EMPTY_BOX, borderwidth=1, width=5, height=2, state=NORMAL, name="%s-%s" % (ligne,colonne) )
				btn.bind("<Button-1>", lambda event : self.onclick(event))
				btn.grid(row=ligne, column=colonne)


	def disableInterface(self):
		for child in self.body_center.winfo_children():
			child['state'] = DISABLED

	def onclick(self, event):
		widget = event.widget
		winner = {"player": None}
		coord = self.game.valid_column(widget._name)
		if coord['index']:
			if self.who_plays == 1 and not widget['state'] == DISABLED:
				if not "played" in widget._name:
					widget.config(bg=self.player_1_color)
					widget['state'] = DISABLED
					self.game.set_grid(coord['index'],self.player_1)
					winner = self.game.win(player= self.player_1 if self.who_plays == 1 else self.player_2)
					self.who_plays = 1 if self.who_plays== 2 else 2
			elif self.who_plays == 2 and not widget['state'] == DISABLED:
				if not "played" in widget._name:
					widget.config(bg=self.player_2_color)
					widget['state'] = DISABLED
					self.game.set_grid(coord['index'],self.player_2)
					winner = self.game.win(player= self.player_1 if self.who_plays == 1 else self.player_2)
					self.who_plays = 1 if self.who_plays== 2 else 2
		#check who win
		if winner['player']:

			winner['player'].increment_score
			self.player_1_score.config(text= self.player_1.score)
			self.player_2_score.config(text = self.player_2.score)
			self.disableInterface()


class Connexion():

    def __init__(self):
    	self.color_player_1 = "#fdcb6e"
    	self.color_player_2 = "#00b894"
    	self.root = Tk()
    	self.root.title("Connexion")
    	self.root.maxsize(400,300)
    	mainFrame = Frame(self.root, padx=10, pady=20, bg="#b2bec3", width=600, height=300 )
    	mainFrame.pack( expand=Y,fill="both")

    	Label(mainFrame, text="Connexion", bg="#b2bec3", fg="#0984e3", font="Helvetica 30 bold").grid(row=0, column=1)

    	Frame(mainFrame, height=20, bg="#b2bec3").grid(row=1)
    	Label(mainFrame, text="Player 1 's name : ", fg="#0984e3", bg="#b2bec3", font="Helvetica 12 bold").grid(row=2, column=0, sticky=(E))
    	Label(mainFrame, text="Player 2 's name : ", fg="#0984e3", bg="#b2bec3", font="Helvetica 12 bold").grid(row=3, column=0, sticky=(E))

    	self.player_one = StringVar()
    	self.player_two = StringVar()

    	Entry(mainFrame, width=30, justify="center", textvariable=self.player_one).grid(row=2, column=1, sticky=(W))
    	Entry(mainFrame, width=30, justify="center", textvariable=self.player_two).grid(row=3, column=1, sticky=(W))

    	btn1 = Button(mainFrame, bg=self.color_player_1, name="player_1_color", font="Helvetica 10 bold")
    	btn1.bind("<Button-1>", lambda event : self.get_color(event))
    	btn1.grid(pady=(0,5),row=2, column=1, sticky=E)

    	btn2 = Button(mainFrame, bg=self.color_player_2, name="player_2_color", font="Helvetica 10 bold")
    	btn2.bind("<Button-1>", lambda event : self.get_color(event))
    	btn2.grid(pady=(0,5),row=3, column=1, sticky=E)

    	self.who_start = IntVar()

    	Radiobutton(mainFrame, text="Player 1", variable=self.who_start, bg="#b2bec3", value=1).grid(row=4, column=1)
    	Radiobutton(mainFrame, text="Player 2", variable=self.who_start, bg="#b2bec3", value=2).grid(row=5, column=1)

    	Button(mainFrame, text="Jouer", width=20,fg="#0984e3", bd=2, font="Helvetica 10 bold", command=self.play).grid(pady=(10,0),row=6, column=1, sticky=W)

    def get_color(self, event):
    	player = event.widget._name
    	color = askcolor()[1]
    	if player == "player_1_color":
    		self.color_player_1 = color
    	elif player == "player_2_color":
    		self.color_player_2 = color
    	event.widget.config(bg= color)

    def load(self):
        self.root.mainloop()

    def play(self):
        if not self.player_one.get() == "" and not self.player_two.get() == "" and not self.who_start.get() == 0:
            self.root.destroy()
            puissance4 = Puissance4Graphique(self.player_one.get(), self.player_two.get(), self.color_player_1, self.color_player_2, self.who_start.get())


if __name__ == '__main__':
	
	Connexion().load()