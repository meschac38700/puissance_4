from Utilities import Utilities
class Game():

	_EMPTY_BOX = "-"

	def __init__(self, player_one, player_two, p_row=6, p_col=7):
		self.player_one = player_one
		self.player_two = player_two
		self.current_player = None
		self.grid = []
		self.set_row_and_col(col=p_col, row=p_row)

	def set_row_and_col(self, col, row):
		self._COLUMN = 7 if col <= 7 or (col - row) <= 0 or  (col - row) > 1 else col
		self._ROW = 6 if row <= 6 or (col - row) <= 0 or  (col - row) > 1 else row

	def initialize_grid(self):
		self.grid = []
		for i in range(self._ROW):
			tmp_line = []
			for j in range(self._COLUMN):
				tmp_line.append(self._EMPTY_BOX)

			self.grid.append(tmp_line)



	def display_grid(self):
		Utilities.display_column_letters(self, espace=" ")
		for i in range(self._ROW):
			for j in range(self._COLUMN):
				print(self.grid[i][j], end=" ")
			print()
		print()

	def grid_is_full(self):
		for i in range(self.ROW):
			for j in range(self.COLUMN):
				if self.grid[i][j] == self._EMPTY_BOX:
					return False
		return True

	@property
	def ROW(self):
		return self._ROW

	@property
	def COLUMN(self):
		return self._COLUMN


	def set_grid(self, coord, player) :
		self.grid[coord[0]][coord[1]] = player.SIGN

	def valid_column(self,  column):

	    if not column == "":
	    	column = column.split("-") if "-" in column else column
			#In the case, we play with the graphic mode
	    	if len(column)>= 2 and column[0].isdigit() and column[1].isdigit():
	    		return self.valid_coordinate(column)

			# In the case, we play with the console mode (the player gonna enter a letter only)
	    	if column.isalpha() and column.upper() in Utilities.get_good_column_letters(self) :
	    		column_index = Utilities.char_to_index(column)
	    		for i in reversed(range(self._ROW)):
	    			if self.grid[i][column_index] == self._EMPTY_BOX:
	    				return {"index": [i, column_index]}
	    return {"index": None}

	#coord = rowCol => [0,0], [0,1], [0,3]  etc...
	def valid_coordinate(self, coord):
		row_index = int(coord[0])
		col_index = int(coord[1])
		if row_index == len(self.grid)-1 :
			return {"index":[row_index, col_index]}
		elif self.grid[row_index+1][col_index] != self._EMPTY_BOX:
			return {"index":[row_index, col_index]}
		else:
			return {"index": None}


	def win_horizontally(self, player):
		for line in self.grid:
			if Utilities.four_in_row(line, player):
				return True
		return False

	def win_vertically(self, player):

		for i in range(self.COLUMN):
			column = []
			for j in range(self.ROW):
				column.append(self.grid[j][i])
			if Utilities.four_in_row(column, player):
				return True
		return False

	def win_left_diagonally(self, player):
		all_left_diagonals = Utilities.get_all_left_diagonally(self)
		for d in all_left_diagonals:
			if Utilities.four_in_row(d, player):
				return True
		return False


	def win_right_diagonally(self, player):
		all_right_diagonals = Utilities.get_all_right_diagonally(self)
		for d in all_right_diagonals:
			if Utilities.four_in_row(d, player):
				return True
		return False

	def win(self, player):
		winner = {"player": None, "direction":None}
		#check if grid is full
		if self.grid_is_full():
			winner = {"player": "No winner", "direction":None}
		if self.win_horizontally(player):
			winner = {"player": player, "direction": "horizontally"}
		elif self.win_vertically(player):
			winner = {"player": player, "direction": "vertically"}
		elif self.win_left_diagonally(player):
			winner = {"player": player, "direction": "left diagonally"}
		elif self.win_right_diagonally(player):
			winner = {"player": player, "direction": "right diagonally"}

		return winner
