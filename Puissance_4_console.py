from Game import Game
from Player import Player

class Puissance4():

	"""
		color print variables
	"""
	BLACK			= "\033[0;30m" 
	RED				= "\033[0;31m" 
	GREEN			= "\033[0;32m" 
	BROW_ORANGE		= "\033[0;33m" 
	BLUE			= "\033[0;34m"
	PURPLE			= "\033[0;35m" 
	CYAN			= "\033[0;36m"
	LIGHT_GREY		= "\033[0;37m"
	
	BLACK_GREY		= "\033[1;30m" 
	LIGHT_RED		= "\033[1;31m" 
	LIGHT_GREEN		= "\033[1;32m" 
	YELLOW			= "\033[1;33m" 
	LIGHT_BLUE		= "\033[1;34m"
	LIGHT_PURPLE	= "\033[1;35m" 
	LIGHT_CYAN		= "\033[1;36m"
	WHITE			= "\033[1;37m"

	NC				= "\033[0m"


	def __init__(self):
		nb_player = 0
		while nb_player != 1 and nb_player != 2:
			try:
				nb_player = int(input(self.LIGHT_BLUE+"How many player ?:\n>>> "+self.NC))
				break
			except:
				print(self.LIGHT_RED+'Enter an integer value'+self.NC)
		
		player_name = input(self.LIGHT_BLUE+"Please enter your name:\n>>> "+self.NC)
		
		alert_message = self.YELLOW+"A single character otherwise it is the first character of the word entered that will be taken"+self.NC
		
		sign = input(self.LIGHT_BLUE+"Please enter "+ player_name +"'s symbol: (only one char !) "+self.NC + "ex: X:\n"+alert_message+"\n>>> ")[0].upper()
		
		self.player_1 = Player(name=player_name, SIGN=sign)
		
		if nb_player == 1 :
			sign = "O" if self.player_1.SIGN == "X" else "X"
			self.player_2 = Player(name="Jhon Doe", SIGN=sign, computer=True)
		else:
			player_name = input(self.LIGHT_BLUE+"Please enter second player's name:\n>>> "+self.NC)

			alert_message2 = self.LIGHT_RED + "The sign of "+ player_name + " must be different from that of "+ self.player_1.get_name() + ""+self.NC
			tmp_counter = 0
			while sign == self.player_1.SIGN:
				if tmp_counter > 0:
					print(alert_message2)
				sign = input(self.LIGHT_BLUE+"Please enter "+ player_name +"'s symbol: (only one char !) "+self.NC + "ex: X:\n"+alert_message+"\n>>> ")[0].upper()
				tmp_counter += 1
			self.player_2 = Player(name=player_name, SIGN=sign)

		self.game = Game(self.player_1, self.player_2)
		
		print(self.PURPLE+"The game will be played between the "+self.NC+self.BROW_ORANGE+ self.player_1.get_name()+self.NC +" and "+ self.LIGHT_BLUE+self.player_2.get_name()+self.NC)
		print(self.YELLOW+"Good luck to each of you"+self.NC)
		print(self.RED+"Let's play"+self.NC)

	@classmethod
	def infos_final_score(cls, player_1, player_2 ):
		p = None
		if player_1.score > player_2.score:
			p = "\033[1;32m"+player_1.get_name() + " : " + str(player_1.score)+"\033[0m - \033[1;31m"+player_2.get_name() + " : " + str(player_2.score)+"\033[0m"
		elif player_2.score > player_1.score:
			p = "\033[1;31m"+player_1.get_name() + " : " + str(player_1.score)+"\033[0m - \033[1;32m"+player_2.get_name() + " : " + str(player_2.score)+"\033[0m"
		else:
			p = "\033[1;37m"+player_1.get_name() + " : " + str(player_1.score)+"\033[0m - \033[1;37m"+player_2.get_name() + " : " + str(player_2.score)+"\033[0m"
		return p

	@classmethod
	def replay(cls):
		answer = None
		while answer != "o" and answer != "n":
			answer = input("Would you like to replay ? (o/n): ").lower()
		return True if answer == "o" else False

	def play(self):
		self.game.initialize_grid()
		first_player = None
		while first_player != "1" and first_player != "2" :
			first_player = input("Who want start ? (1|2): ")

		player_1_turn = True if int(first_player) == 1 else False

		while True:	
			self.game.display_grid()

			if player_1_turn:
				column = self.player_1.propose_column(self.game)
			else:
				column = self.player_2.propose_column(self.game)
				
			
			who_played = self.player_1 if player_1_turn else self.player_2
			player_1_turn = not player_1_turn

			self.game.set_grid(column, who_played)
			self.game.display_grid()


			winner = self.game.win(who_played)
			#check if someone won
			if winner['player'] :
				if winner['player'] == "No winner":
					print(self.LIGHT_RED+ "Egalite !"+self.NC)
				else:
					winner['player'].increment_score
					print(self.LIGHT_PURPLE+"Congratulation to "+self.LIGHT_BLUE+winner['player'].get_name()+ self.NC+", he won the game ! with a "+ self.NC+self.LIGHT_BLUE+winner['direction'] +" direction"+self.NC )
				replay_answer = Puissance4.replay()
				if replay_answer:
					self.play()
				break
			

g = Puissance4()
g.play()
print("Final score !")
print(Puissance4.infos_final_score(g.player_1,g.player_2) )